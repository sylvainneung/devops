'use strict';

// DB
const DB_HOST = process.env.DB_HOST || 'database-1.cmcuoamu3usb.us-east-1.rds.amazonaws.com';
const DB_USER = process.env.DB_USER || 'admin';
const DB_PASSWORD = process.env.DB_PASSWORD || 'admindevops';
const DB_NAME = process.env.DB_NAME || 'cours_devops';
const DB_PORT = process.env.DB_PORT || 3306;

const mysql      = require('mysql');

const connection = mysql.createConnection({
    host     : DB_HOST,
    user     : DB_USER,
    password : DB_PASSWORD,
    database : DB_NAME,
    port: DB_PORT
});


module.exports = connection;

