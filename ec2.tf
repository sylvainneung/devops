resource "aws_key_pair" "esgi" {
  key_name = "esgi"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDktBCGXb+f9un4njd84rRCojeTlFomdp6uoq0WaRZfYBan25DWqxLJjSd8gQ/E3O+KgA7073SylxjjfZF41HhhYqbSHvu2FSFEDYI8rwsQ9v1ksFCikEcx9zHanLAh0ElJbahEheaM166gFxYqcpyhmaPCXUskD3j+uNKr3BmcF3AMkNZugVriMghCp5IpaOkZPM5tanQRwI6pMxZs/bD+72nPiACm6BmYDl9NQ3Mb+xMVMVIWCmxfGnXhGd83Ph6rIWsAxygqly589IH6j6Kx618jJ9VTml7VYmTWm4fIZffLd8jmKvueOB3Qizvqkd3m9YjaQafi/XqlCVCNrc8H sylvainjoly@sylvain"
}

resource "aws_instance" "devops_esgi_production" {
  key_name = "esgi"
  ami = "ami-0357d42faf6fa582f"
  instance_type = "t2.micro"
  tags = {
    Name = "ESGI_Devops_Prod"
  }

  #connection {
    #host = self.public_ip
    #type = "ssh"
    #user = "ubuntu"
    #private_key = "ssh-keys/id_rsa-aws"
  #}

  #provisioner "remote-exec" {
    #inline= [
      #"sudo yum update -y",
      #"sudo yum install -y docker",
      #"sudo service docker start",
      #"sudo docker run -d -p 80:3000 registry.gitlab.com/sylvainneung/devops:latest npm run start"]

    #inline = [
      #"echo ${aws_instance.devops_esgi_production.private_ip}"]
  #}

  #security_groups = ["${aws_security_group.allow_ssh_for_terraform.name}", "SshForTerraform","default"]
  security_groups = ["SshForTerraform","AppSecurity", "default"]
}

resource "aws_instance" "devops_esgi_staging" {
  key_name = "esgi"
  ami = "ami-0357d42faf6fa582f"
  instance_type = "t2.micro"
  tags = {
    Name = "ESGI_Devops_Staging"
  }

  #provisioner "local-exec" {
    #command = "curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash && . ~/.nvm/nvm.sh && nvm install node && node -e 'console.log(process.version)' "
  #}
  #security_groups = ["${aws_security_group.allow_ssh_for_terraform.name}","SshForTerraform","default"]
  security_groups = ["SshForTerraform","AppSecurity", "default"]

}

# not used
resource "aws_security_group" "allow_ssh_for_terraform" {
 name        = "allow_ssh_for_terraform"
 description = "Allow SSH traffic"

 ingress {
   from_port = 22
   to_port = 22
   protocol = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
}
