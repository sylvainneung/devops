'use strict';


const express       = require('express');
const bodyparser    = require('body-parser');
const connection = require('./dbConn');

const app = express();
app.use(bodyparser.json());

const port = process.env.PORT || 3000;

app.get('/', async (req,res,next) => {

    res.status(200).json({
        "hELLO": "world"
    })
});

app.get('/users', async (req,res,next) => {
    let data = [];
    connection.query('SELECT * FROM user2', function (error, results, fields) {
        if (error) throw error;
        console.log(results[0]);
        data = results[0];
        res.status(200).json({
            "data" : data
        })
    });

});



app.listen(port, () => console.log(`App start on ${port}`));

