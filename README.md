# Help 


Use ami-08c757228751c5335 for the ec2 instance


````shell script
$ terraform -v 


$ terraform init 


$ terraform plan 


$ terraform apply


$ terraform apply

````

```shell script
$ ssh -i ssh-keys/id_rsa_aws ubuntu@ec2-15-236-123-85.eu-west-3.compute.amazonaws.com
```

ssh keys for the pair keys terraform

````shell script
$  ssh-keygen -t rsa -f id_rsa_aws


(useless now)
$ ssh-keygen -f ~/.ssh/id_rsa.pub -m 'PEM' -e > public.pem (use this to connect, so make name match with your terraform config)

$ chmod 400 public.pem )

````


scp -i ssh-keys/id_rsa_aws test.txt ubuntu@ec2-15-236-202-106.eu-west-3.compute.amazonaws.com:/home/ubuntu


    ansible -i inventory.ini all -a "hostname" -U ec2-user

-> ssh-add


### Resources

sudo docker run -d -p 80:3000 registry....:dev  

https://stackoverflow.com/questions/51442736/how-to-deploy-web-application-to-aws-instance-from-gitlab-repository

https://carlchenet.com/debuter-avec-aws-et-terraform-deployer-une-instance-ec2/

https://www.ybrikman.com/writing/2015/11/11/running-docker-aws-ground-up/

